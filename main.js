var player = {level: 1, damage: 10};
var enemy = {level: 1, health: 100, death: 0};
var experience = {xp: 0};
var gold = {g: 0};
var powerup1 = {damage: 4, amount: 0};
var powerup2 = {damage: 10,amount: 0};
var powerup3 = {damage: 20, amount: 0};
var timer1;



function attack(){
    enemy.health = enemy.health - player.damage;


    if (enemy.health <= 0){
        health();
        death();
        image();
    }
    
    document.getElementById("enemyHealth").innerHTML = "Health: " + enemy.health;

}

function health(){

    if (enemy.death >= 50)
        enemy.health = 2500 + 100 * enemy.death;
    else
     enemy.health = 100 + 50 * enemy.death;
}

function death(){

    enemy.death++;
    document.getElementById("enemyKills").innerText = "Enemies killed " + enemy.death;
    experience.xp =  0.5 * enemy.death;
    player.damage = 10 + 3 * enemy.death;

    if(enemy.death % 2 === 0){

        player.level++;
    }

    console.log("Enemy died!");
    document.getElementById("playerLevel").innerHTML = "Player level: " + player.level;
    document.getElementById("ExpGained").innerHTML = "You gained: " + experience.xp + " Experience";
    money();
}

function players(){
    var playerName = document.getElementById("playername").value;
    document.getElementById("player1").innerHTML = playerName;
    document.getElementById("removed").innerHTML = "";

}

function money(){
    gold.g =  gold.g + 15;
    document.getElementById("goldmoney").innerHTML = "Your money is: " + gold.g + "gold";

}

function shiv() {
    if (gold.g >= 150) {
        gold.g = gold.g - 150;
        powerup1.amount = powerup1.amount + 1;
        player.damage = player.damage + powerup1.damage;
        document.getElementById("powerup1").innerHTML = "shiv: " + powerup1.amount;
        document.getElementById("goldmoney").innerHTML = "Ur money is: " + gold.g + "gold";
    }
}

function poisonshiv() {
    if (gold.g >= 250) {
        gold.g = gold.g - 250;
        powerup2.amount = (powerup2.amount + 1);
        powerup1.amount = powerup1.amount - 1;
        player.damage = player.damage - powerup1.damage + powerup2.damage;
        document.getElementById("powerup2").innerHTML = "Poisoned shiv: " + powerup2.amount;
        document.getElementById("powerup1").innerHTML = "shiv: " + powerup1.amount;
        document.getElementById("goldmoney").innerHTML = "Ur money is: " + gold.g + "gold";
    }
}

function Dot() {
    if (enemy.death <= 0) {
        document.getElementById("img2").style.background  = "url('blobhidden.gif')";
        document.getElementById("img2").style.width = "220px";
        document.getElementById("img2").style.height = "220px";
        document.getElementById("img2").style.backgroundRepeat = "no-repeat";
        document.getElementById("img2").style.backgroundSize = "contain";
        document.getElementById("enemyname").innerHTML = "Hidden Goomba";

        document.getElementById("img1").style.background  = "url('Hammerhidden.gif')";
        document.getElementById("img1").style.width = "300px";
        document.getElementById("img1").style.height = "200px";
        document.getElementById("img1").style.backgroundRepeat = "no-repeat";
        document.getElementById("img1").style.backgroundSize = "contain";

    }

    if (gold.g >= 400) {
        gold.g = gold.g - 400;
        powerup3.amount = powerup3.amount + 1;
        dotdamage();
        document.getElementById("powerup3").innerHTML = "Dot amounts: " + powerup3.amount;
        document.getElementById("goldmoney").innerHTML = "Ur money is: " + gold.g + "gold";
        document.getElementById("enemyHealth").innerHTML = "Health: " + enemy.health;
    }
}

function dotdamage() {
    enemy.health = enemy.health - 20;
    if (enemy.health <= 0) {
        health();
        death();
        image();
    }

    timer1 = setTimeout(dotdamage, 1000);
    document.getElementById("enemyHealth").innerHTML = "Health: " + enemy.health;


}

function image(background,width,height,backgroundRepeat,backgroundSize){

    if (enemy.death >= 40) {
        document.getElementById("img1").style.background  = "url('Hammer50.gif')";
        document.getElementById("img1").style.width = "300px";
        document.getElementById("img1").style.height = "200px";
        document.getElementById("img1").style.backgroundRepeat = "no-repeat";
        document.getElementById("img1").style.backgroundSize = "contain";

    }
    else if (enemy.death >= 30) {
        document.getElementById("img1").style.background  = "url('Hammer1.gif')";
        document.getElementById("img1").style.width = "300px";
        document.getElementById("img1").style.height = "200px";
        document.getElementById("img1").style.backgroundRepeat = "no-repeat";
        document.getElementById("img1").style.backgroundSize = "contain";

    }
    else if (enemy.death >= 20) {
        document.getElementById("img1").style.background  = "url('Hammer3.gif')";
        document.getElementById("img1").style.width = "300px";
        document.getElementById("img1").style.height = "200px";
        document.getElementById("img1").style.backgroundRepeat = "no-repeat";
        document.getElementById("img1").style.backgroundSize = "contain";

    }
    else if (enemy.death >= 10) {
        document.getElementById("img1").style.background  = "url('Hammer4.gif')";
        document.getElementById("img1").style.width = "300px";
        document.getElementById("img1").style.height = "200px";
        document.getElementById("img1").style.backgroundRepeat = "no-repeat";
        document.getElementById("img1").style.backgroundSize = "contain";

    }
    else {
        document.getElementById("img1").style.background =  "url('Hammer.gif')";
        document.getElementById("img1").style.width = "300px";
        document.getElementById("img1").style.height = "200px";
        document.getElementById("img1").style.backgroundRepeat = "no-repeat";
        document.getElementById("img1").style.backgroundSize = "contain";
    }

     if (enemy.death >= 50) {
        document.getElementById("img2").style.background  = "url('blob50.gif')";
        document.getElementById("img2").style.width = "220px";
        document.getElementById("img2").style.height = "220px";
        document.getElementById("img2").style.backgroundRepeat = "no-repeat";
        document.getElementById("img2").style.backgroundSize = "contain";
         document.getElementById("enemyname").innerHTML = "Amazing blob";
    }
    else if (enemy.death >= 35) {
        document.getElementById("img2").style.background  = "url('blob3.gif')";
        document.getElementById("img2").style.width = "220px";
        document.getElementById("img2").style.height = "220px";
        document.getElementById("img2").style.backgroundRepeat = "no-repeat";
        document.getElementById("img2").style.backgroundSize = "contain";
         document.getElementById("enemyname").innerHTML = "Cool blob";
    }
    else if (enemy.death >= 25) {
        document.getElementById("img2").style.background  = "url('blob0.gif')";
        document.getElementById("img2").style.width = "220px";
        document.getElementById("img2").style.height = "220px";
        document.getElementById("img2").style.backgroundRepeat = "no-repeat";
        document.getElementById("img2").style.backgroundSize = "contain";
         document.getElementById("enemyname").innerHTML = "blob";
    }
    else if (enemy.death >= 10) {
        document.getElementById("img2").style.background  = "url('blob1.gif')";
        document.getElementById("img2").style.width = "220px";
        document.getElementById("img2").style.height = "220px";
        document.getElementById("img2").style.backgroundRepeat = "no-repeat";
        document.getElementById("img2").style.backgroundSize = "contain";
         document.getElementById("enemyname").innerHTML = "Mini blob";
    }
    else {
        document.getElementById("img2").style.background =  "url('blob2.gif')";
        document.getElementById("img2").style.width = "220px";
        document.getElementById("img2").style.height = "220px";
        document.getElementById("img2").style.backgroundRepeat = "no-repeat";
        document.getElementById("img2").style.backgroundSize = "contain";
        document.getElementById("enemyname").innerHTML = "Extra mini blob";
    }
}

